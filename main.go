package main

import (
	"fmt"
	"gitlab.com/k33g/wasmedge-function/vmhelpers"
)

func main() {

	wasmVM, _ := vmhelpers.InitializeVMFromWasmFile("hello-function/hello.wasm")

	wasmVM.ExecuteMainFunction()

	result, _ := wasmVM.ExecuteFunction("hello", "Bob Morane")

	fmt.Println(result)

	wasmVM.Release()

}
