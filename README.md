# ebook go samples wasmedge function

Developers must install the WasmEdge shared library with the same WasmEdge-go release version.

```bash
wget -qO- https://raw.githubusercontent.com/WasmEdge/WasmEdge/master/utils/install.sh | bash -s -- -v 0.9.0
source /home/gitpod/.wasmedge/env 
go mod init wasmedge-func
go get github.com/second-state/WasmEdge-go/wasmedge@v0.9.0
```

## Build and run the program

```bash
go build
./wasmedge-function hello-function/hello.wasm 
```

## VM Helpers

### Factory of wasm VM

```plantuml
class VMHelpers {
  InitializeVMFromWasmFile(wasmPathFile string) (WasmVM, error)
}
```

### WasmVM type

```plantuml
class WasmVM {
  config *wasmedge.Configure
  store *wasmedge.Store
  vm *wasmedge.VM

  writeStringValueToMemory(value string) (int32, *wasmedge.Memory, []byte)
  getStringValueFromResult(wasmFunctionResult []interface{}, mem *wasmedge.Memory, memData []byte) (string, int32)

  ExecuteMainFunction() (interface{}, error)
  ExecuteFunction(functionName, stringParam string) (string, error)
  Release()

}
```
